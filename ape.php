<?php
require_once("animal.php");
class Ape extends Animal
{
    public function __construct($name)
    {
        $this->name = $name;
        $this->legs = 2;
    }
    //public $legs = 2;
    public function yell()
    {
        return "Auoooo";
    }
}