<?php
class Animal
{
    public $name;
    public $cold_blooded;
    public $legs;
    public function __construct($name)
    {
        $this->name = $name;
        $this->legs = 4;
        $this->cold_blooded = "no";
    }
}